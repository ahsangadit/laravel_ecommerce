
# Setup
* `composer install`
* `php artisan migrate --seed`
* `npm install`
* `npm run dev`
* `php artisan storage:link`


# Credentials
* `john@doe.com / secret (role:superadmin)`
* `admin@doe.com / secret (role:admin)`
* `clerk@doe.com / secret (role:user)`